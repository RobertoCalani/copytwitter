<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $categorias = [
            'Cooperación',
            'Lanzamiento de productos',
            'Compras y Adquisiciones'
        ];

        foreach ($categorias as $categoria) {
            \App\Models\Categoria::create([
                'nombre'    =>  $categoria
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
