<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $regiones = [
            'Argentina',
            'Estados Unidos',
            'Portugal',
            'Italia',
            'Brasil',
            'Colombia',
            'España',
            'Canadá'
        ];

        foreach ($regiones as $region) {
            \App\Models\Region::create([
                'nombre'    =>  $region
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
