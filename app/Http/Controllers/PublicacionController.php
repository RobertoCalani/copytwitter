<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Publicacion;
use App\Models\Enlace;
use App\Models\Referencia;
use Illuminate\Support\Facades\DB;

class PublicacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $publicaciones = Publicacion::with('categoria')->with('region')->with('referencias')->with('enlaces')->get();
        return response()->json($publicaciones);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Log::info($request->all());

        try {
            DB::beginTransaction();
            $publicacion = Publicacion::create($request->publicacion);
            if (count($request->enlaces) > 0) {
                foreach ($request->enlaces as $enlace) {
                    Enlace::create([
                        'url' => $enlace,
                        'publicacion_id' => $publicacion->id
                    ]);
                }
            }

            if (count($request->referencias) > 0) {
                foreach ($request->referencias as $referencia) {
                    Referencia::create([
                        'url' => $referencia,
                        'publicacion_id' => $publicacion->id
                    ]);
                }
            }
            DB::commit();
            return response()->json([
                'publicacion' => $publicacion
            ]);
        } catch (Throwable $e) {
            DB::rollback();     
            return response()->json([
                'publicacion' => $publicacion
            ], 502);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Publicacion $publicacion)
    {
        return response()->json($publicacion);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Publicacion $publicacion)
    {
        $publicacion->fill($request->post())->save();
        return response()->json([
            'publicacion' => $publicacion
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
