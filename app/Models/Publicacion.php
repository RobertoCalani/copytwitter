<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Publicacion extends Model
{
    use HasFactory;

    protected $table = 'publicacion';
    protected $fillable = ['contenido', 'region_id', 'categoria_id'];
    protected $casts = [
        'created_at' => 'datetime:d M H:i'
     ];
     
    public function categoria()
    {
        return $this->belongsTo(Categoria::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function enlaces()
    {
        return $this->hasMany(Enlace::class, 'publicacion_id', 'id');
    }

    public function referencias()
    {
        return $this->hasMany(Referencia::class);
    }
}