# CopyTwitter

El proyecto esta construido con Sail/Laravel, se debe tener instalado Docker

## Instalacion

Clonamos el repositorio [repositorio](https://gitlab.com/RobertoCalani/copytwitter.git/)

```bash
git clone https://gitlab.com/RobertoCalani/copytwitter.git
```
Nos novemos a la carpeta
```bash
cd copytwitter
```
## Levantar servidor
Instalamos las dependencias
```
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/var/www/html \
    -w /var/www/html \
    laravelsail/php81-composer:latest \
    composer install --ignore-platform-reqs
```
Copiamos .env
```
cp .env.example .env
```

(Opcional) Si tiene problemas con los permisos
```
sudo chown -R $USER:www-data .
sudo chgrp -R www-data storage bootstrap/cache
sudo chmod -R ug+rwx storage bootstrap/cache
```
Levantamos Sail
```
./vendor/bin/sail up -d
```
Instalamos dependencias de NPM
```
./vendor/bin/sail npm install
```
Construimos los css y js para VUE
```
./vendor/bin/sail npm run build
```
Generamos KEY de Laravel
```
./vendor/bin/sail artisan key:generate
```
Corremos las migraciones de Laravel
```
./vendor/bin/sail artisan migrate
```
## Referencias
[Curso Vue.js 3 desde cero](https://www.youtube.com/playlist?list=PLYAyQauAPx8l7G8lTbSv23UTAwbVORY1r)

[3 formas de escribir componentes en Vue3
](https://www.youtube.com/watch?v=GfEMMbSyMpE&ab_channel=GarajedeIdeas)

[28 - Laravel ahora usa Vite ¿Que ocurrió con Webpack?
](https://www.youtube.com/watch?v=4gxxWTe3pVA&ab_channel=CodersFree)

[https://laracasts.com/discuss/channels/vue/is-there-anyway-to-have-vue-reload-a-page](https://laracasts.com/discuss/channels/vue/is-there-anyway-to-have-vue-reload-a-page)

[https://laravel.com/docs/9.x/sail#installing-composer-dependencies-for-existing-projects](https://laravel.com/docs/9.x/sail#installing-composer-dependencies-for-existing-projects)

[https://stackoverflow.com/questions/736513/how-do-i-parse-a-url-into-hostname-and-path-in-javascript](https://stackoverflow.com/questions/736513/how-do-i-parse-a-url-into-hostname-and-path-in-javascript)

[https://stackoverflow.com/questions/40899532/how-to-pass-a-value-from-vue-data-to-href](https://stackoverflow.com/questions/40899532/how-to-pass-a-value-from-vue-data-to-href)

[https://stackoverflow.com/questions/8498592/extract-hostname-name-from-string](https://stackoverflow.com/questions/8498592/extract-hostname-name-from-string)
